<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'welcome' => 'Welcome',
    'chooselang' => 'Choose your language',
    'login' => 'Login',
    'email' => 'Email Address',
    'password' => 'Password',
    'remember' => 'Remember Me',
    'logout' => 'Logout',
    'home' => 'Home',
    'company' => 'Company',
    'employee' => 'Employee',
    'moreinfo' => 'More Info',
    'name' => 'Name',
    'website' => 'Website',
    'create' => 'Create',
    'create_company' => 'Create',
    'list_of_employees' => 'List of Employees',
    'company_logo' => 'Company Logo',
    'close' => 'Close',
    'edit_company' => 'Edit Company',
    'submit' => 'Submit',
    'confirm_delete_company' => 'Do you want to delete this company?',
    'format' => 'Format',
    'add' => 'Add',
    'first_name' => 'First Name',
    'last_name' => 'Last Name',
    'phone' => 'Phone',
    'add_employee' => 'Add Employee',
    'edit_employee' => 'Edit Employee',


    'company_created' => 'Company registered successfully!',
    'company_updated' => 'Company updated successfully!',
    'company_deleted' => 'Company deleted successfully!',

    'employee_created' => 'Employee registered successfully!',
    'employee_updated' => 'Employee updated successfully!',
    'employee_deleted' => 'Employee deleted successfully!',


];
