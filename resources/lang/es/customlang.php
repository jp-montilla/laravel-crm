<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'welcome' => 'Bienvenido',
    'chooselang' => 'Elige tu idioma',
    'login' => 'Iniciar sesión',
    'email' => 'Dirección de correo electrónico',
    'password' => 'Contraseña',
    'remember' => 'Recuérdame',
    'logout' => 'Cerrar sesión',
    'home' => 'Vete a casa',
    'company' => 'Empresa',
    'employee' => 'Empleado',
    'moreinfo' => 'Más Información',
    'name' => 'Nombre',
    'website' => 'Sitio web',
    'create' => 'Crear',
    'create_company' => 'Crear empresa',
    'list_of_employees' => 'Lista de empleados',
    'company_logo' => 'Logo de la compañía',
    'close' => 'Cerca',
    'edit_company' => 'Editar empresa',
    'submit' => 'Enviar',
    'confirm_delete_company' => '¿Quieres eliminar esta empresa?',
    'format' => 'Formato',
    'add' => 'Añadir',
    'first_name' => 'Nombre de pila',
    'last_name' => 'Apellido',
    'phone' => 'Teléfono',
    'add_employee' => 'Agregar empleado',
    'edit_employee' => 'Editar empleado',


    'company_created' => '¡Empresa registrada correctamente!',
    'company_updated' => '¡Empresa actualizada con éxito!',
    'company_deleted' => '¡Empresa eliminada correctamente!',

    'employee_created' => 'Empleado registrado con éxito!',
    'employee_updated' => 'Empleado actualizado con éxito!',
    'employee_deleted' => 'Empleado eliminado con éxito!',

];
