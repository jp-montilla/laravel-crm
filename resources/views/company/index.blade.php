@extends('layouts.admin')

@section('mainContent')

<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
		<h1 class="inline m-0 text-dark">{{__('customlang.company')}} <span><button id="companyAddBtn" class="inline btn btn-primary btn-sm"><i class="fas fa-plus"></i> {{__('customlang.create')}}</button></span></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('/') }}">{{__('customlang.home')}}</a></li>
          <li class="breadcrumb-item active">{{__('customlang.company')}}</li>
        </ol>
      </div>
    </div>
  </div>
</div>

<section class="content">
	<div class="container-fluid">
		@if(Session::get('success'))
		<div class="alert alert-primary alert-dismissible fade show" role="alert">
		  {{ Session::get('success') }}
		   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		    <span aria-hidden="true">&times;</span>
		  </button>
		</div>
		@endif
		<div class="card">

			<div class="card-body">
				<table id="company_tbl" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>Logo</th>
							<th>{{__('customlang.name')}}</th>
							<th>{{__('customlang.email')}}</th>
							<th>{{__('customlang.website')}}</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						@foreach($companies as $company)
							<tr>
								<td><img src="{{ asset('images/'.$company->logo) }}" style="max-height: 200px"></td>
								<td>{{ $company->name }}<br/></td>
								<td>{{ $company->email }}</td>
								<td>{{ $company->website }}</td>
								<td>
									
									<button id="viewCompany_{{ $company->id }}" class="viewCompany btn btn-primary btn-sm"><i class="fas fa-folder"></i></button>


									<button id="editCompany_{{ $company->id }}"class="editCompany btn btn-info btn-sm"><i class="fas fa-pencil-alt"></i></b>

									<form action="{{ url('company' , $company->id ) }}" method="POST">
									    {{ csrf_field() }}
									    {{ method_field('DELETE') }}
									    <button class="btn btn-danger btn-sm" onclick="return confirm('Do you want to delete this company?')"><i class="fas fa-trash"></i></button>
									</form>



								</td>
							</tr>
						@endforeach
					</tbody>
					<tfoot>
						<tr>
							<th>Logo</th>
							<th>{{__('customlang.name')}}</th>
							<th>{{__('customlang.email')}}</th>
							<th>{{__('customlang.website')}}</th>
							<th></th>
						</tr>
					</tfoot>
				</table>
			</div>
			<!-- /.card-body -->
		</div>
	</div>
</section>


<!-- CREATE MODAL -->
<div id="companyAdd" class="modal fade {{ count($errors) > 0 && Session::get('action') == 'create' ? 'with_errors' : '' }}" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      	<h5>{{__('customlang.create_company')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	@if(count($errors) > 0 && Session::get('action') == 'create')
      	<div id="errors_add">
	      	<div class="alert alert-danger">
	      		@foreach($errors->all() as $error)
	      			<li>{{ $error }}</li>
	      		@endforeach
	      	</div>
      	</div>
      	@endif
		<form action="{{ url('company/store') }}" id="addFormCompany" method="POST" enctype="multipart/form-data">
		  {{ csrf_field() }}
	      <div class="form-group">
	        <label>{{__('customlang.name')}}</label>
	        <input type="text" name="name" class="form-control" value="{{ old('name') }}"placeholder="Enter name">
	      </div>
	      <div class="form-group">
	        <label>{{__('customlang.email')}}</label>
	        <input type="email" name="email" class="form-control"  value="{{ old('email') }}"placeholder="Enter email">
	      </div>
	      <div class="form-group">
	        <label>{{__('customlang.website')}}</label>
	        <input type="text" name="website" class="form-control"  value="{{ old('website') }}"placeholder="Enter website">
	        <small>Format: https://www.google.com/</small>
	      </div>
	     
	      <div class="form-group">
            <label for="exampleInputFile">Logo</label>
            <div class="input-group">
                <input type="file" name="logo" id="exampleInputFile">
            </div>
          </div>
		  
      </div>
      <div class="modal-footer">
      	<button type="submit" id="companySubmit" class="btn btn-primary">{{__('customlang.submit')}}</button>
      	</form>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('customlang.close')}}</button>
      </div>
    </div>
  </div>
</div>
<!-- CREATE MODAL -->


<!-- VIEW MODAL -->
<div id="viewCompanyModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h4 id="comp_name" class="modal-title"></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
        	<div class="col-md-8">
        		<h5>{{__("customlang.email")}}:</h5>
        		<p id="comp_email">
        		</p>
        		<h5>{{__("customlang.website")}}:</h5>
        		<p id="comp_website">
        		</p>
        		<h5>{{__("customlang.list_of_employees")}}:</h5>
        		<div id="comp_employee"></div>
        	</div>
        	<div  class="col-md-4">
        		<h6 class="text-center">{{__('customlang.company_logo')}}</h6>
        		<div class="" id="company_logo"></div>
        	</div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('customlang.close')}}</button>
      </div>
    </div>
  </div>
</div>
<!-- VIEW MODAL -->


<!-- EDIT MODAL -->
<div id="companyEdit" class="modal fade {{ count($errors) > 0 && Session::get('action') == 'update' ? 'with_errors' : '' }}" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      	<h5>{{__('customlang.edit_company')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	@if(count($errors) > 0 && Session::get('action') == 'update')
      	<div id="errors_edit">
	      	<div class="alert alert-danger">
	      		@foreach($errors->all() as $error)
	      			<li>{{ $error }}</li>
	      		@endforeach
	      	</div>
      	</div>
      	@endif
		<form action="{{ url('company/update') }}"  method="POST" enctype="multipart/form-data">
		  {{ csrf_field() }}
		  <input type="hidden" id="company_id" name="company_id">
	      <div class="form-group">
	        <label>{{__('customlang.name')}}</label>
	        <input type="text" id="name_edit" name="name_edit" class="form-control" value="{{ old('name_edit') }}"placeholder="Enter name">
	      </div>
	      <div class="form-group">
	        <label>{{__('customlang.email')}}</label>
	        <input type="email" id="email_edit" name="email_edit" class="form-control"  value="{{ old('email_edit') }}"placeholder="Enter email">
	      </div>
	      <div class="form-group">
	        <label>{{__('customlang.website')}}</label>
	        <input type="text" id="website_edit" name="website_edit" class="form-control"  value="{{ old('website_edit') }}"placeholder="Enter website">
	        <small>{{__('customlang.format')}}: https://www.google.com/</small>
	      </div>
	     
	      <div class="form-group">
            <label for="exampleInputFile">Logo</label>
            <div class="input-group">
                <input type="file" name="logo_edit" id="exampleInputFile">
            </div>
          </div>
          <div id="viewLogoEdit"></div>
		  
      </div>
      <div class="modal-footer">
      	<button type="submit" id="companySubmit" class="btn btn-primary">{{__('customlang.submit')}}</button>
      	</form>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('customlang.close')}}</button>
      </div>
    </div>
  </div>
</div>



@endsection