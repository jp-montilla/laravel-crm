@extends('layouts.admin')

@section('mainContent')

<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">{{__('customlang.employee')}} <span><button id="employeeAddBtn" class="inline btn btn-primary btn-sm"><i class="fas fa-plus"></i> {{__('customlang.add')}}</button></span></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('/') }}">{{__('customlang.home')}}</a></li>
          <li class="breadcrumb-item active">{{__('customlang.employee')}}</li>
        </ol>
      </div>
    </div>
  </div>
</div>

<section class="content">
	<div class="container-fluid">
		@if(Session::get('success'))
		<div class="alert alert-primary alert-dismissible fade show" role="alert">
		  {{ Session::get('success') }}
		   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		    <span aria-hidden="true">&times;</span>
		  </button>
		</div>
		@endif
		<div class="card">
			<div class="card-body">
				<table id="company_tbl" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>{{__('customlang.first_name')}}</th>
							<th>{{__('customlang.last_name')}}</th>
							<th>{{__('customlang.company')}}</th>
							<th>{{__('customlang.email')}}</th>
							<th>{{__('customlang.phone')}}</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						@foreach($employees as $key => $employee)
							<tr>
								<td>{{ $employee->first_name }}</td>
								<td>{{ $employee->last_name }}</td>
								<td id="{{ $comp_id_post[$key] }}">{{ $company_name[$key] }}</td>
								<td>{{ $employee->email }}</td>
								<td>{{ $employee->phone }}</td>
								<td>

									<button id="viewEmployee_{{ $employee->id }}" class="viewEmployee btn btn-primary btn-sm"><i class="fas fa-folder"></i></button>


									<button id="editEmployee_{{ $employee->id }}"class="editEmployee btn btn-info btn-sm"><i class="fas fa-pencil-alt"></i></b>
									
									<form action="{{ url('employee' , $employee->id ) }}" method="POST">
									    {{ csrf_field() }}
									    {{ method_field('DELETE') }}
									    <button class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')"><i class="fas fa-trash"></i></button>
									</form>

								</td>
							</tr>
						@endforeach
					</tbody>
					<tfoot>
						<tr>
							<th>{{__('customlang.first_name')}}</th>
							<th>{{__('customlang.last_name')}}</th>
							<th>{{__('customlang.company')}}</th>
							<th>{{__('customlang.email')}}</th>
							<th>{{__('customlang.phone')}}</th>
							<th></th>
						</tr>
					</tfoot>
				</table>
			</div>
			<!-- /.card-body -->
		</div>
	</div>
</section>







<!-- CREATE MODAL -->
<div id="employeeAdd" class="modal fade {{ count($errors) > 0 && Session::get('action') == 'create' ? 'with_errors' : '' }}" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      	<h5>{{__('customlang.add_employee')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	@if(count($errors) > 0 && Session::get('action') == 'create')
      	<div id="errors_add">
	      	<div class="alert alert-danger">
	      		@foreach($errors->all() as $error)
	      			<li>{{ $error }}</li>
	      		@endforeach
	      	</div>
      	</div>
      	@endif
		<form action="{{ url('employee/store') }}" method="POST" >
		  {{ csrf_field() }}
	      <div class="form-group">
	        <label>{{__('customlang.first_name')}}</label>
	        <input type="text" name="first_name" class="form-control" value="{{ old('first_name') }}"placeholder="Enter name">
	      </div>
	       <div class="form-group">
	        <label>{{__('customlang.last_name')}}</label>
	        <input type="text" name="last_name" class="form-control" value="{{ old('last_name') }}"placeholder="Enter name">
	      </div>
	       <div class="form-group">
	        <label>{{__('customlang.company')}}</label>
	        <select class="form-control" name="company" value="{{ old('company') }}">
			  @foreach($companies as $company)
			  	<option value="{{ $company->id }}">{{ $company->name }}</option>
			  @endforeach
			</select>
	      </div>
	      <div class="form-group">
	        <label>{{__('customlang.email')}}</label>
	        <input type="email" name="email" class="form-control"  value="{{ old('email') }}"placeholder="Enter email">
	      </div>
	      <div class="form-group">
	        <label>{{__('customlang.phone')}}</label>
	        <input type="tel" id="phone" class="form-control" name="phone" value="{{ old('phone') }}" placeholder="09123456789" pattern="[0][0-9]{10}">
	        <small>{{__('customlang.format')}}: 09123456789</small>
	      </div>
		  
      </div>
      <div class="modal-footer">
      	<button type="submit" id="companySubmit" class="btn btn-primary">{{__('customlang.submit')}}</button>
      	</form>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('customlang.close')}}</button>
      </div>
    </div>
  </div>
</div>
<!-- CREATE MODAL -->



<!-- VIEW MODAL -->
<div id="viewEmployeeModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h4 id="full_name" class="modal-title"></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<h5>{{__('customlang.first_name')}}:</h5>
		<div id="first_name"></div>
		<h5>{{__('customlang.last_name')}}:</h5>
		<div id="last_name"></div>
		<h5>{{__('customlang.company')}}:</h5>
		<div id="company"></div>
		<h5>{{__('customlang.email')}}:</h5>
		<div id="email"></div>
		<h5>{{__('customlang.phone')}}:</h5>
		<div id="phone_num"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('customlang.close')}}</button>
      </div>
    </div>
  </div>
</div>
<!-- VIEW MODAL -->





<!-- EDIT MODAL -->
<div id="employeeEdit" class="modal fade {{ count($errors) > 0 && Session::get('action') == 'update' ? 'with_errors' : '' }}" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      	<h5>{{__('customlang.edit_employee')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	@if(count($errors) > 0 && Session::get('action') == 'update')
      	<div id="errors_add">
	      	<div class="alert alert-danger">
	      		@foreach($errors->all() as $error)
	      			<li>{{ $error }}</li>
	      		@endforeach
	      	</div>
      	</div>
      	@endif
		<form action="{{ url('employee/update') }}" method="POST" >
		  {{ csrf_field() }}
		  <input type="hidden" id="employee_id" name="employee_id">
	      <div class="form-group">
	        <label>{{__('customlang.first_name')}}</label>
	        <input type="text" id="emp_fname" name="emp_first_name" class="form-control" value="{{ old('emp_first_name') }}"placeholder="Enter name">
	      </div>
	       <div class="form-group">
	        <label>{{__('customlang.last_name')}}</label>
	        <input type="text" id="emp_lname" name="emp_last_name" class="form-control" value="{{ old('emp_last_name') }}"placeholder="Enter name">
	      </div>
	       <div class="form-group">
	        <label>{{__('customlang.company')}}</label>
	        <select class="form-control" id="emp_company" name="emp_company">
			  @foreach($companies as $company)
			  	<option value="{{ $company->id }}">{{ $company->name }}</option>
			  @endforeach
			</select>
	      </div>
	      <div class="form-group">
	        <label>{{__('customlang.email')}}</label>
	        <input type="email" id="emp_email" name="emp_email" class="form-control"  value="{{ old('emp_email') }}"placeholder="Enter email">
	      </div>
	      <div class="form-group">
	        <label>{{__('customlang.phone')}}</label>
	        <input type="tel" id="emp_phone" class="form-control" name="emp_phone" value="{{ old('emp_phone') }}" placeholder="09123456789" pattern="[0][0-9]{10}">
	        <small>{{__('customlang.format')}}: 09123456789</small>
	      </div>
		  
      </div>
      <div class="modal-footer">
      	<button type="submit" id="companySubmit" class="btn btn-primary">{{__('customlang.submit')}}</button>
      	</form>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('customlang.close')}}</button>
      </div>
    </div>
  </div>
</div>
<!-- EDIT MODAL -->














@endsection