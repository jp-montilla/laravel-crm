<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<style>
	body{
		font: .875rem/1.4 Nunito Sans,system-ui,-apple-system,Segoe UI,Roboto,Ubuntu,Cantarell,Noto Sans,sans-serif;
	}
</style>
<body>
	<h1>Company Created Successfully</h1>
	<h2>Details</h2>
	<p>Company Name: {{ $company->name }}</p>
	<p>Company Email: {{ $company->email }}</p>
	<p>Company Website: <a href="{{ $company->email }} target='_blank'"> {{ $company->website }}</a></p>
	<p>Created at: {{ $company->created_at }}</p>

</body>
</html>