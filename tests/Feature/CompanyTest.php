<?php

namespace Tests\Feature;

use App\User;
use App\Company;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CompanyTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function a_company_can_be_added_through_the_form(){
        $this->actingAs(factory(User::class)->create());
        $company = factory('App\Company')->make();
        $this->post('company/store', $company->toArray());
        $this->assertCount(1, Company::all());
    }

    /** @test */
    public function a_company_cannot_be_added_through_the_form_without_name(){
        $this->actingAs(factory(User::class)->create());
        $company = factory('App\Company')->make(['name'=>'']);
        $this->post('company/store', $company->toArray())->assertSessionHasErrors('name');
    }

    /** @test */
    public function a_company_cannot_be_added_through_the_form_without_email(){
        $this->actingAs(factory(User::class)->create());
        $company = factory('App\Company')->make(['email'=>'']);
        $this->post('company/store', $company->toArray())->assertSessionHasErrors('email');
    }

    /** @test */
    public function a_company_cannot_be_added_through_the_form_with_invalid_email(){
        $this->actingAs(factory(User::class)->create());
        $company = factory('App\Company')->make(['email'=>'not email']);
        $this->post('company/store', $company->toArray())->assertSessionHasErrors('email');
    }

    /** @test */
    public function a_company_cannot_be_added_through_the_form_without_website(){
        $this->actingAs(factory(User::class)->create());
        $company = factory('App\Company')->make(['website'=>'']);
        $this->post('company/store', $company->toArray())->assertSessionHasErrors('website');
        // $this->assertCount(0, Company::all());
    }

    /** @test */
    public function a_company_cannot_be_added_through_the_form_with_invalid_website(){
        $this->actingAs(factory(User::class)->create());
        $company = factory('App\Company')->make(['website'=>'not a url']);
        $this->post('company/store', $company->toArray())->assertSessionHasErrors('website');
    }

    /** @test */
    public function a_company_cannot_be_added_through_the_form_with_invalid_logo_dimensions(){
        $this->actingAs(factory(User::class)->create());
        $company = factory('App\Company')->make(['logo'=> UploadedFile::fake()->image('avatar.jpg', 90, 100)]);
        $this->post('company/store', $company->toArray())->assertSessionHasErrors('logo');
    }

    /** @test */
    public function a_company_cannot_be_added_through_the_form_with_wrong_logo_format(){
        $this->actingAs(factory(User::class)->create());
        $company = factory('App\Company')->make(['logo'=> UploadedFile::fake()->create('document.pdf')]);
        $this->post('company/store', $company->toArray())->assertSessionHasErrors('logo');
    }

    /** @test */
    public function unathenticated_user_cannot_create_a_new_company(){
        $company = factory('App\Company')->make();
        $this->post('company/store', $company->toArray())->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_user_can_read_all_company(){
        $this->actingAs(factory(User::class)->create());
        $company = factory('App\Company')->create();
        $response = $this->get('/company');
        $response->assertSee($company->name);
        $response->assertSee($company->email);
        $response->assertSee($company->website);
    }

    /** @test */
    public function authenticated_user_can_update_the_company(){
        $this->actingAs(factory(User::class)->create());
        $company = factory('App\Company')->create();
        $company->name = "Updated Name";
        $response = $this->put('company/update', $company->toArray());
        $response->assertSessionHasNoErrors();
    }

    /** @test */
    public function unauthenticated_user_cannot_update_the_company(){
        $company = factory('App\Company')->create();
        $company->name = "Updated Name";
        $response = $this->put('company/update', $company->toArray());
        $response->assertStatus(405);
    }

    /** @test */
    public function authenticated_user_can_delete_the_company(){
        $this->actingAs(factory(User::class)->create());
        $company = factory('App\Company')->create();
        $response = $this->delete('company/'.$company->id);
        $response->assertSessionHasNoErrors();
    }

    /** @test */
    public function unauthenticated_user_cannot_delete_the_company(){
        $this->actingAs(factory(User::class)->create());
        $company = factory('App\Company')->create();
        $response = $this->delete('company/'.$company->id);
        $response->assertStatus(302);
    }
}
