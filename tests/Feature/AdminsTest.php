<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function only_logged_in_users_can_see_the_dashboard()
    {
        $response = $this->get('/dashboard')->assertRedirect('/login');
    }

    /** @test */
    public function authenticad_users_can_see_the_dashboard()
    {
        $this->actingAs(factory(User::class)->create());
        $response = $this->get('/dashboard')->assertOk();
    }

    /** @test */
    public function only_logged_in_users_can_see_the_company_list()
    {
        $response = $this->get('/company')->assertRedirect('/login');
    }

    /** @test */
    public function authenticad_users_can_see_the_company_list()
    {
        $this->actingAs(factory(User::class)->create());
        $response = $this->get('/company')->assertOk();
    }

    /** @test */
    public function only_logged_in_users_can_see_the_employee_list()
    {
        $response = $this->get('/employee')->assertRedirect('/login');
    }

    /** @test */
    public function authenticad_users_can_see_the_employee_list()
    {
        $this->actingAs(factory(User::class)->create());
        $response = $this->get('/employee')->assertOk();
    }


}
