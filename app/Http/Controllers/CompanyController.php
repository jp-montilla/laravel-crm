<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use Session;
use DB;
use Auth;
use App\Mail\CompanyCreated;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = DB::table('companies')->paginate(10);
        return view('company.index')->with('companies', $companies);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Session::put('action', 'create');
        $request->validate([
            'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|dimensions:min_width=100,min_height=100',
            'name' => 'required',
            'email' => 'required|email',
            'website' => 'required|url',
        ]);

        $company = new Company;

        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $imageName = date('YmdHis').".".$file->getClientOriginalExtension();
            $file->move(public_path('images/'), $imageName);
        }
        
        $company->logo = $imageName;
        $company->name = $request->name;
        $company->email = $request->email;
        $company->website = $request->website;

        $company->save();

        \Mail::to(Auth::user()->email)->send(new CompanyCreated($company));

        return redirect('/company')->with('success', __('customlang.company_created'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Company::find($id);
        $company_employees = $company->employees;
        echo json_encode($company_employees);
    }

    /**

     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        Session::put('action', 'update');
        $validation = '';
        if ($request->hasFile('logo_edit')) {
            $validation = 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:min_width=100,min_height=100';
        }

        $request->validate([
            'logo_edit' => $validation,
            'name_edit' => 'required',
            'email_edit' => 'required|email',
            'website_edit' => 'required|url',
        ]);


        $company = Company::find($request->company_id);

        if ($request->hasFile('logo_edit')) {
            $file = $request->file('logo_edit');
            $imageName = date('YmdHis').".".$file->getClientOriginalExtension();
            $file->move(public_path('images/'), $imageName);
            $company->logo = $imageName;
        }
        
        $company->name = $request->name_edit;
        $company->email = $request->email_edit;
        $company->website = $request->website_edit;
        $company->save();
        return redirect('/company')->with('success', __('customlang.company_updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::find($id);
        $company->delete();
        return redirect()->back()->with('success', __('customlang.company_deleted'));
    }
}
