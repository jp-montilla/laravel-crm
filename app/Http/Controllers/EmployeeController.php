<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Company;
use Session;
use DB;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees_get = Employee::all();
        $this->data['companies'] = [];
        $this->data['comp_id_post'] = [];
        foreach ($employees_get as $emp) {
            $this->data['company_name'][] = $emp->company->name;
            $this->data['comp_id_post'][] = $emp->company->id;
        }
        $this->data['employees'] = DB::table('employees')->paginate(10);
        $this->data['companies'] = Company::all();
        return view('employee.index')->with($this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Session::put('action', 'create');
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'company' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
        ]);

        $employee = new Employee;

        
        $employee->first_name = $request->first_name;
        $employee->last_name = $request->last_name;
        $employee->company_id = $request->company;
        $employee->email = $request->email;
        $employee->phone = $request->phone;
        $employee->save();

        return redirect('/employee')->with('success', __('customlang.employee_created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        Session::put('action', 'update');
        $request->validate([
            'emp_first_name' => 'required',
            'emp_last_name' => 'required',
            'emp_company' => 'required',
            'emp_email' => 'required|email',
            'emp_phone' => 'required',
        ]);
        $employee = Employee::find($request->employee_id);
        $employee->first_name = $request->emp_first_name;
        $employee->last_name = $request->emp_last_name;
        $employee->company_id = $request->emp_company;
        $employee->email = $request->emp_email;
        $employee->phone = $request->emp_phone;
        $employee->save();
        return redirect('/employee')->with('success', __('customlang.employee_updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = Employee::find($id);
        $employee->delete();
        return redirect()->back()->with('success', __('customlang.employee_deleted'));
    }
}
