$(document).ready(function(){
  $('#company_tbl').DataTable({
    "paging": true,
    "pageLength": 10,
    "lengthChange": false,
    "searching": false,
    "ordering": true,
    "info": true,
    "autoWidth": false,
    "responsive": true,
  });

  $('#employee_tbl').DataTable({
    "paging": true,
    "pageLength": 10,
    "lengthChange": false,
    "searching": false,
    "ordering": true,
    "info": true,
    "autoWidth": false,
    "responsive": true,
  });

  $('#companyAddBtn').click(function(){
      $('#companyAdd').modal('show');
    });

  if($('#companyEdit').hasClass('with_errors')) {
    $('#companyEdit').modal('show');
  }

  if($('#companyAdd').hasClass('with_errors')) {
    $('#companyAdd').modal('show');
  }

  $('#companyAdd').on("hide.bs.modal", function(){
    $('#errors_add').html('');
    $('#errors_edit').html('');
  });

  $('#companyEdit').on("hide.bs.modal", function(){
    $('#errors_add').html('');
    $('#errors_edit').html('');
  });
  $('#viewCompanyModal').on('hide.bs.modal', function(){
    $('#comp_employee').html('');
  });

  $('.viewCompany').click(function(){
    var id = $(this).attr('id');
    var id = id.split("_")[1];
    var data = $(this).closest('tr').children('td').map(function(){
      if($(this).is(':first-child')){
       return $("img", $(this)).attr("src");
      }
      else{
        return $(this).text();
      }
    });
    $('#comp_name').text(data[1]);
    $('#comp_email').html('<p class="form-control" style="background: #ececec">'+data[2]+'</p>');
    $('#comp_website').html(' <p class="form-control" style="background: #ececec">'+data[3]+'</p>');
    
    fetchRecords(id);

    $('#company_logo').html('<img src="'+data[0]+'" width="100%" />');
    $('#viewCompanyModal').modal('show');
  });



  $('.editCompany').click(function(){
    var id = $(this).attr('id');
    var id = id.split("_")[1];
    var data = $(this).closest('tr').children('td').map(function(){
      if($(this).is(':first-child')){
       return $("img", $(this)).attr("src");
      }
      else{
        return $(this).text();
      }
    });
    $('#company_id').val(id);
    $('#name_edit').val(data[1]);
    $('#email_edit').val(data[2]);
    $('#website_edit').val(data[3]);
    $('#viewLogoEdit').html('<img src="'+data[0]+'" width="50%" />');
    $('#companyEdit').modal('show');
  });





  $('#employeeAddBtn').click(function(){
    $('#employeeAdd').modal('show');
  });

  if($('#employeeEdit').hasClass('with_errors')) {
    $('#employeeEdit').modal('show');
  }


  $('#employeeAdd').on("hide.bs.modal", function(){
    $('#errors_add').html('');
    $('#errors_edit').html('');
  });

  $('#employeeEdit').on("hide.bs.modal", function(){
    $('#errors_add').html('');
    $('#errors_edit').html('');
  });



  if($('#employeeAdd').hasClass('with_errors')) {
    $('#employeeAdd').modal('show');
  }

  $('#viewCompanyModal').on('hide.bs.modal', function(){
    $('#comp_employee').html('');
  });


  $('.viewEmployee').click(function(){
    var id = $(this).attr('id');
    var id = id.split("_")[1];
    var data = $(this).closest('tr').children('td').map(function(){
      return $(this).text();
    });
    console.log(data[4]);
    $('#full_name').html('<h4>'+ data[0]+ ' '+ data[1] +'</h4>')
    $('#first_name').html('<p class="form-control" style="background: #ececec; font-weight: bold">'+data[0]+'</p>');
    $('#last_name').html('<p class="form-control" style="background: #ececec; font-weight: bold">'+data[1]+'</p>');
    $('#company').html('<p class="form-control" style="background: #ececec; font-weight: bold">'+data[2]+'</p>');
    $('#email').html('<p class="form-control" style="background: #ececec; font-weight: bold">'+data[3]+'</p>');
    $('#phone_num').html('<p class="form-control" style="background: #ececec; font-weight: bold">'+data[4]+'</p>');

    $('#viewEmployeeModal').modal('show');
  });



  $('.editEmployee').click(function(){
    var id = $(this).attr('id');
    var id = id.split("_")[1];
    var selected_comp = 0;
    var data = $(this).closest('tr').children('td').map(function(){

      if($(this).is(':nth-child(3)')){
        selected_comp = $(this).attr('id');
      }
      return $(this).text();
        
    });
    $('#employee_id').val(id);
    $('#emp_fname').val(data[0]);
    $('#emp_lname').val(data[1]);
    $('#emp_company').val(selected_comp);
    $('#emp_email').val(data[3]);
    $('#emp_phone').val(data[4]);
    $('#employeeEdit').modal('show');
  });








  function getCompanyId(){

  }










  function fetchRecords(id){
    $.ajax({
      url: 'company/'+id,
      type: 'get',
      dataType: 'json',
      success:function(response){
        var html = '';
        if (response.length != 0) {
          html += '<ul>';
          $.map(response, function(emp){
            html += '<li>'+ emp['first_name']+' '+emp['last_name'] + '</li>';
          });

          html += '</ul>';
          
        }
        else{
          html += '<p class="text-danger">None</p>';
        }
        $('#comp_employee').html(html);
      },

    });
  }












});


