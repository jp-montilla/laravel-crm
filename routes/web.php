<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('lang/{locale}', function ($locale) {
	session()->put('locale', $locale);
	return redirect()->back();
});

Auth::routes([
  'register' => false, // Registration Routes...
  'reset' => false, // Password Reset Routes...
  'verify' => false, // Email Verification Routes...
]);

// Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function() {
	Route::get('/dashboard', 'DashboardController@index');

	Route::group(['prefix' => 'company'], function(){
		Route::get('/', 'CompanyController@index');
		Route::get('/{id}', 'CompanyController@show');
		Route::post('/store', 'CompanyController@store');
		Route::post('/update', 'CompanyController@update');
		Route::delete('/{id}', 'CompanyController@destroy');
	});

	Route::group(['prefix' => 'employee'], function(){
		Route::get('/', 'EmployeeController@index');
		Route::post('/store', 'EmployeeController@store');
		Route::post('/update', 'EmployeeController@update');
		Route::delete('/{id}', 'EmployeeController@destroy');
	});
});
