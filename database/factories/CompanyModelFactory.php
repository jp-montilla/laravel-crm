<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Company;
use Faker\Generator as Faker;
use Illuminate\Http\UploadedFile;

$factory->define(Company::class, function (Faker $faker) {
    return [
        'name' => 'Test Company',
        'email' => 'testemail@email.com',
        'website' => 'http://google.com',
        'logo' => UploadedFile::fake()->image('avatar.jpg', 100, 100),
    ];
});
